#!/usr/bin/env python
# coding: utf8

from ..utils import osw
import wave
import io

def strip_wav_header(input_file, output_file):
    header_size = 0x46
    with io.open(input_file, 'rb') as i,\
            io.open(output_file, 'wb') as o:
        i.seek(header_size)
        o.write(i.read())
        