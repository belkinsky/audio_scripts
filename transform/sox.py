#!/usr/bin/env python
# coding: utf8

from ..utils import osw
from typing import NamedTuple

global_sox_options = '--show-progress --buffer 524288' #--multi-threaded seems not efficient


class FileInfo(NamedTuple):
    sample_rate: int
    samples_total: int


def sox_file_info(filename):
    sample_rate = int(osw.shell_exec(f'soxi -r "{osw.shell_escape(filename)}"'))
    samples_total = int(osw.shell_exec(f'soxi -s "{osw.shell_escape(filename)}"'))
    return FileInfo(sample_rate, samples_total)


def sox_transform(source, destination, transformation, overwrite_existing=False, niceness=0):
    sox_options = global_sox_options
    if isinstance(source,list):
        sox_options = sox_options + ' --combine mix-power'
        sources_text = osw.quoted_filenames_list(source)
    else:
        sources_text = f'"{osw.shell_escape(source)}"'

    if not overwrite_existing:
        sox_options = sox_options + ' --no-clobber'

    # todo: fix insecure method (dangerous shell)
    sox_command = f'time sox {sox_options} {sources_text} "{osw.shell_escape(destination)}" {transformation}'
    osw.shell_exec(sox_command, niceness=niceness)
    return destination


def sox_transform_tmp(tmpdir, source_filename, index, transformation, niceness=0):
    transformed_filename = osw.create_tmp_filename(tmpdir, source_filename, outindex=index)
    sox_transform(source_filename, transformed_filename, transformation, niceness=niceness)
    return transformed_filename
