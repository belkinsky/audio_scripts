#!/usr/bin/env python
# coding: utf8
import os

import logging as Log
Log.basicConfig(format="[%(thread)d] %(message)s", level=Log.DEBUG)


def shell_exec(string_cmd, stdin_content = None, ignore_error = False, niceness=0):
    string_cmd = f'nice -n {int(niceness)} ' + string_cmd
    Log.debug(f'Executing: {string_cmd}')
    file = os.popen(string_cmd)
    if stdin_content != None:
        output = file.communicate(input=stdin_content.encode())[0]
    else:
        output = file.read()
    ret_code = file.close()
    if ret_code != None and not ignore_error:
        raise ChildProcessError(f'Return code is {ret_code} (not good). Output is "{output}"')
    Log.debug(output)
    return output


_shell_path_translate = str.maketrans({
#    '(': r'\(',
#    ')': r'\)',
#    '>': r'\>',
#    '&': r'\&',
    '$': r'\$',
})


def shell_escape(filename):
    return filename.translate(_shell_path_translate)


def path_split(source):
    (path, name_ext) = os.path.split(source)
    (name, ext) = os.path.splitext(name_ext)
    return (path, name, ext)


def path_root(path):
    return path_split(path)[0]


def path_name(path):
    return path_split(path)[1]


def path_ext(path):
    return path_split(path)[2]


def path_edit(source, new_path = None, new_name = None, new_ext = None):
    (path, name, ext) = path_split(source)
    #Log.debug(f'  path "{source}" includes: path="{path}" name="{name}" ext="{ext}"')
    if new_path != None:
        path = new_path
    if new_name != None:
        name = new_name
    if new_ext != None:
        ext = new_ext
    result = os.path.join(path, name+ext)
    #Log.debug(f'  path result is "{result}"')
    return result


def path_add_suffix(filepath, suffix):
    return path_edit(filepath, None, path_name(filepath)+suffix, None)


def path_reroot(new_root, orig_root, path_to_move):
    #Log.debug(f'new_root:"{new_root}", orig_root:"{orig_root}", path_to_move:"{path_to_move}"')
    result = os.path.join(new_root, os.path.relpath(path_to_move, orig_root))
    #Log.debug(f'  result:"{result}""')
    return result


def create_tmp_dir(tmp_root, tagname):
    tmp_dir = os.path.join(tmp_root, tagname)
    shell_exec(f'rm -rfd "{shell_escape(tmp_dir)}"') # todo: fix dangerous cleanup
    #if not os.path.exists(tmp_dir):
    shell_exec(f'mkdir -p "{shell_escape(tmp_dir)}"')
    return tmp_dir


def create_tmp_filename(tmpdir, source_filename, tag='', outindex=0):
    return path_add_suffix(path_edit(source_filename, tmpdir), f'_{tag}{outindex}')


def cleanup_tmp_files(tmpdir, files_list):
    # todo: ensure that files are under tmpdir
    for f in files_list:
        shell_exec(f'rm "{shell_escape(f)}"')


def quoted_filenames_list(filenames, separator=' '):
    escaped_filenames = [shell_escape(p) for p in filenames]
    return '"'+'" "'.join(escaped_filenames)+'"'

def walk_audio_single_dir(single_dir_or_file):
    if(os.path.isdir(single_dir_or_file)):
        for dirpath, _, files in os.walk(single_dir_or_file):
            for f in files:
                ext = path_ext(f).upper()
                if ext == '.MP3' or ext == '.WAV': # todo: use external source search criteria
                    yield (single_dir_or_file, dirpath, f)
    elif os.path.isfile(single_dir_or_file):
        yield (path_root(single_dir_or_file), path_root(single_dir_or_file), path_name(single_dir_or_file))


def walk_audio(source_dirs_and_files):
    if isinstance(source_dirs_and_files, list):
        for d in source_dirs_and_files:
            for result in walk_audio_single_dir(d):
                yield result
    else:
        for result in walk_audio_single_dir(source_dirs_and_files):
            yield result


def walk_audio_out_dir(source_dir, output_dir):
    Log.debug(f'Walking source_dir:"{source_dir}"')
    for source_base, source_dir, f in walk_audio(source_dir):
        #Log.debug(f'  source_base:"{source_base}", source_dir:"{source_dir}", file:"{f}"')
        source_file = os.path.join(source_dir, f)
        output_file = path_reroot(output_dir, source_base, source_file)
        output_root = path_split(output_file)[0]
        yield (source_base, source_dir, f, output_root)        
