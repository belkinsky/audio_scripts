#!/usr/bin/env python
# coding: utf8

"""
    Python oneliner script usage.

    USAGE: python -m spleeter-scripts {separate_chunked} ...
"""

import sys
import warnings

from .commands import create_argument_parser

def command_name_to_entrypoint(cmd):
    if cmd == 'separate_chunked':
        from .commands.separate_chunked import entrypoint
    elif cmd == 'mixdown':
        from .commands.mixdown import entrypoint
    elif cmd == 'remix':
        from .commands.remix import entrypoint
    else:
        raise RuntimeError(f'Unexpected command "{cmd}"')
    return entrypoint

def main(argv):
    """ Audio scripts runner. Parse provided command line arguments
    and run entrypoint for required command (separate_chunked at the moment).

    :param argv: Provided command line arguments.
    """
    parser = create_argument_parser()
    arguments = parser.parse_args(argv[1:])

    entrypoint = command_name_to_entrypoint(arguments.command)
    entrypoint(arguments)


def entrypoint():
    """ Command line entrypoint. """
    warnings.filterwarnings('ignore')
    main(sys.argv)


if __name__ == '__main__':
    entrypoint()
