#!/usr/bin/env python
# coding: utf8

import collections
from typing import NamedTuple
from ..utils import osw
import os
from ..transform import sox
from ..transform import wav

import logging as Log
Log.basicConfig(format="[%(thread)d] %(message)s", level=Log.DEBUG)

global_sox_options = '--show-progress --buffer 524288 -v 4' #--multi-threaded seems not efficient


class ChunkInfo(NamedTuple):
    source: str
    start: int
    duration: int
    skip_on_start: int
    skip_on_end: int
    crossfade_on_start: int
    crossfade_on_end: int


def int_up_to_even(int_number):
    return  int(int_number+1) if int(int_number) & 1 == 1 else int(int_number)


def calc_chunks(source_filename, chunks_dir, samples_total, chunk_duration_samples, crossfade_samples, skip_samples):
    minimal_chunk_payload_samples = 4000
    crossfade_samples = int_up_to_even(crossfade_samples)
    samples_total = int(samples_total)
    skip_samples = int(skip_samples)
    chunk_duration_samples = int(chunk_duration_samples)
    start = 0
    chunks = {}
    while start < samples_total:
        end = start + chunk_duration_samples
        if end > samples_total \
                or end + crossfade_samples*2 + skip_samples*2 + minimal_chunk_payload_samples > samples_total:
            end = samples_total
            crossfade_on_end_samples = 0
            skip_on_end_samples = 0
        else:
            crossfade_on_end_samples = crossfade_samples
            skip_on_end_samples = skip_samples

        if start > 0:
            crossfade_on_start_samples = crossfade_samples
            skip_on_start_samples = skip_samples
        else:
            crossfade_on_start_samples = 0
            skip_on_start_samples = 0
        
        actual_duration = int(end - start)
        percent_done = int(end/samples_total*100)

        chunk_path = osw.path_edit(source_filename, chunks_dir, f'{osw.path_name(source_filename)}_from{int(start)}-to{int(end)}-dur{actual_duration}-{percent_done}percent', '.wav')
        Log.debug(f'Chunk path:"{chunk_path}", crossfade_on_start:{crossfade_on_start_samples}, crossfade_on_end:{crossfade_on_end_samples}, skip_on_start:{skip_on_start_samples}, skip_on_end:{skip_on_end_samples}')

        chunks[chunk_path] = ChunkInfo(source_filename, start, actual_duration, skip_on_start_samples, skip_on_end_samples, crossfade_on_start_samples, crossfade_on_end_samples)

        future_skip_on_start_samples = 0 if crossfade_on_end_samples ==0 and skip_on_end_samples == 0 else skip_samples

        start += actual_duration - future_skip_on_start_samples - skip_on_end_samples - crossfade_on_end_samples
    return chunks

#todo: move to ..transform.sox
def audio_split(chunks, source = None, niceness=0):
    for chunk_filename, chunk in chunks.items():
        osw.shell_exec(f'time sox {global_sox_options} "{osw.shell_escape(source or chunk.source)}" "{osw.shell_escape(chunk_filename)}" trim {int(chunk.start)}s {int(chunk.duration)}s',
            niceness=niceness)


def cleanup_files(chunks, if_any=False):
    for chunk_filename, _ in chunks.items():
        if if_any and not os.path.exists(chunk_filename):
            pass  # do not try to remove absent file
        else:
            osw.shell_exec(f'rm "{osw.shell_escape(chunk_filename)}"')


def call_spleeter_separate(input_files_str_or_list, output_dir, preset_name, niceness=0):
    if input_files_str_or_list is str:
        input_files = osw.shell_escape(input_files_str_or_list)
        input_list = input_files_str_or_list.split(' ')
    else:
        input_files = osw.quoted_filenames_list(input_files_str_or_list)
        input_list = input_files_str_or_list

    spleeter_command = f'spleeter separate -i {input_files} -p {preset_name} -d 864000 -o "{osw.shell_escape(output_dir)}" --verbose'
    osw.shell_exec(spleeter_command, niceness=niceness)

    output = {}
    for inp in input_list:
        output_chunk_dir = os.path.join(output_dir, osw.path_name(inp))
        Log.debug(f'Assuming spleeter result is here:{output_chunk_dir}')

        result_files = []
        for root, _, files in os.walk(output_chunk_dir):
            for f in files:
                if osw.path_ext(f).upper() == '.wav'.upper():
                    result_file = os.path.join(root, f)
                    result_files.append(result_file)
                    Log.debug(f'  assuming result file:{result_file}')
                    sox.sox_file_info(result_file)
        
        #todo: validate result - check count and suffixes?
        if len(result_files) == 0:
            raise ChildProcessError(f'Cannot find a result of spleeter from command "{spleeter_command}". The result is expected to be in "{output_chunk_dir}"')
        
        output[inp] = {'output_chunk_dir':output_chunk_dir, 'result_files':result_files}

    return output


def regroup_stem_chunks(separated_results, chunks):
    output = {}
    for origin_chunk_filename, results in separated_results.items():
        origin_chunk = chunks[origin_chunk_filename]
        #results['output_chunk_dir'] #unused
        for result_file in results['result_files']:
            merged_filename = osw.path_edit(origin_chunk_filename, None, osw.path_name(origin_chunk.source) + osw.path_name(result_file), osw.path_ext(result_file))
            if merged_filename not in output:
                output[merged_filename] = {}

            output[merged_filename][result_file] = ChunkInfo(merged_filename, origin_chunk.start, origin_chunk.duration, origin_chunk.skip_on_start, origin_chunk.skip_on_end, origin_chunk.crossfade_on_start, origin_chunk.crossfade_on_end)

    for merged_filename, out_chunks in output.items():
        Log.debug(f'Stitch stem "{merged_filename}" from the following:')
        for chunk_filename, _ in out_chunks.items():
            Log.debug(f'  filename:{chunk_filename}')
    return output


def stitch_chunks(output_file, chunks, niceness=0):
    sorted_chunks = sorted(chunks.items(), key=lambda kv:kv[1].start)

    is_first_chunk = True
    for filename_info in sorted_chunks:
        filename = filename_info[0]
        info = filename_info[1]
        trim_start = info.skip_on_start
        Log.debug(f'  skip_on_start:{info.skip_on_start}s, skip_on_end:{info.skip_on_end}s')
        trim_duration = int(info.duration - info.skip_on_start - info.skip_on_end)
        trimmed = osw.path_add_suffix(filename, '_trim')

        if trim_duration == info.duration:
            osw.shell_exec(f'mv "{osw.shell_escape(filename)}" "{osw.shell_escape(output_file)}"')
        else:
            osw.shell_exec(f'time sox {global_sox_options} "{osw.shell_escape(filename)}" "{osw.shell_escape(trimmed)}" trim {trim_start}s {trim_duration}s',
                niceness=niceness)
        
            if is_first_chunk:
                osw.shell_exec(f'mv "{osw.shell_escape(trimmed)}" "{osw.shell_escape(output_file)}"')
                is_first_chunk = False
            else:
                output_file_samples = int(osw.shell_exec(f'soxi -s "{osw.shell_escape(output_file)}"'))
                output_file_new = osw.path_add_suffix(output_file, '_new')
                sox_excess = int(info.crossfade_on_start/2)
                osw.shell_exec(f'time sox {global_sox_options} "{osw.shell_escape(output_file)}" "{osw.shell_escape(trimmed)}" "{osw.shell_escape(output_file_new)}" splice -h {output_file_samples}s,{sox_excess}s,0s',
                    niceness=niceness)
                osw.shell_exec(f'rm "{osw.shell_escape(trimmed)}"')
                osw.shell_exec(f'mv -f "{osw.shell_escape(output_file_new)}" "{osw.shell_escape(output_file)}"')

    osw.shell_exec(f'soxi "{osw.shell_escape(output_file)}"') # will print info


def separate_chunked_single_file(audio_filename, output_dir,
    chunk_duration_sec, skip_time_sec,
    crossfade_duration_ms, target_sample_rate,
    preset_name, tmp_root, niceness=0):

    tmp_dir = osw.create_tmp_dir(tmp_root, osw.path_name(audio_filename)+'_separate')

    #convert source to wav with certain samplerate
    audio_filename_wav = osw.path_edit(audio_filename, tmp_dir, osw.path_name(audio_filename) + '_', '.wav')
    Log.debug(f'Unpacked source:{audio_filename_wav}, sample_rate:{target_sample_rate}')
    try:
        osw.shell_exec(f'time sox {global_sox_options} "{osw.shell_escape(audio_filename)}" -r {target_sample_rate} "{osw.shell_escape(audio_filename_wav)}"',
            niceness=niceness)
    except ChildProcessError:
        #use alternative way
        audio_filename_tmp = osw.path_edit(audio_filename, None, osw.path_name(audio_filename_wav) + '_payload', '.mp3')
        wav.strip_wav_header(audio_filename, audio_filename_tmp)
        osw.shell_exec(f'time sox {global_sox_options} "{osw.shell_escape(audio_filename_tmp)}" -r {target_sample_rate} "{osw.shell_escape(audio_filename_wav)}"',
            niceness=niceness)


    #calculate chunks
    file_info = sox.sox_file_info(audio_filename_wav)
    duration_sec = file_info.samples_total / file_info.sample_rate
    chunk_duration_samples = int(chunk_duration_sec * file_info.sample_rate)
    skip_samples = skip_time_sec * file_info.sample_rate
    crossfade_samples = int(crossfade_duration_ms * file_info.sample_rate / 1000)
    Log.debug(f'Sample rate(samples/sec):{file_info.sample_rate}, Samples total:{file_info.samples_total}, Duration(sec):{duration_sec}, Chunk duration(samples):{chunk_duration_samples}')
    chunks = calc_chunks(audio_filename_wav, tmp_dir, file_info.samples_total, chunk_duration_samples, crossfade_samples, skip_samples)

    #split audio into chunks
    audio_split(chunks, niceness=niceness)
    osw.shell_exec(f'rm "{osw.shell_escape(audio_filename_wav)}"')

    #separate for each chunk
    separated_results = call_spleeter_separate(chunks.keys(), tmp_dir, preset_name, niceness=niceness)
    cleanup_files(chunks)

    #calculate chunks to be stitched
    to_be_stitched = regroup_stem_chunks(separated_results, chunks)

    if not os.path.exists(output_dir):
        osw.shell_exec(f'mkdir -p "{osw.shell_escape(output_dir)}"')

    #stitch all chunks
    output_files = []
    for stem, chunks in to_be_stitched.items():
        output_file = osw.path_edit(stem, output_dir)
        output_files.append(output_file)
        stitch_chunks(output_file, chunks, niceness=niceness)
        cleanup_files(chunks, if_any=True)
        #todo:consider yield return the result - output_file

    osw.shell_exec(f'rm -rd "{osw.shell_escape(tmp_dir)}"')
    return output_files


def separate_chunked_path(source_file_or_dir, output_path,
    chunk_duration_sec = 240, skip_time_sec = 1,
    crossfade_duration_ms = 100, target_sample_rate = 44100, preset_name = 'spleeter:4stems',
    tmp_root = './tmp', niceness=0):

    if os.path.isdir(source_file_or_dir):
        for _, source_dir, f, out_dir in osw.walk_audio_out_dir(source_file_or_dir, output_path):
            input_file = os.path.join(source_dir, f)
            files = separate_chunked_single_file(input_file, out_dir,
                chunk_duration_sec, skip_time_sec, crossfade_duration_ms, target_sample_rate, preset_name, tmp_root,
                niceness=niceness)
            for f in files:
                yield f
    else:
        files = separate_chunked_single_file(source_file_or_dir, output_path,
            chunk_duration_sec, skip_time_sec, crossfade_duration_ms, target_sample_rate, preset_name, tmp_root,
            niceness=niceness)
        for f in files:
            yield f


def entrypoint(arguments):
    print(f'arguments = {arguments}')
    output_list = []
    for path in arguments.input_paths:
        output_list.extend(separate_chunked_path(path, arguments.output_path,
            preset_name=arguments.preset_name, tmp_root=arguments.tmp_dir, niceness=arguments.niceness))

    return output_list


if __name__ == "__main__":
    #todo: make tests
    separate_chunked_single_file(
        audio_filename = r'./tests/data/source.wav',
        output_dir = './tmp/out',
        chunk_duration_sec = 0.90703, # 40k samples
        skip_time_sec = 0.06804, # 3k samples
        crossfade_duration_ms = 45.34, # 2k samples
        target_sample_rate = 44100,
        preset_name = 'spleeter:4stems',
        tmp_root = ','
    )
    osw.shell_exec(f'ls -l "./tmp/source/"')
