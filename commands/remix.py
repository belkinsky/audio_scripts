#!/usr/bin/env python
# coding: utf8

from ..utils import osw
from ..transform import sox
from ..commands import create_argument_parser

import os
from enum import Enum
from retrying import retry
from argparse import Namespace
from joblib import Parallel, delayed


from ..commands.separate_chunked import entrypoint as separate_chunked_entrypoint
from ..commands.mixdown import entrypoint as mixdown_entrypoint

import logging as Log
Log.basicConfig(format="[%(thread)d] %(message)s", level=Log.DEBUG)

#todo: move out
class Tag(Enum):
    MasterOutput = 0
    PresetName = 1
    PresetSequence = 2


_preset_remix_fardrums = {
    Tag.PresetName: 'fardrums remix',
    Tag.PresetSequence:[
        'separate_chunked:4stems', # consider to add to preset:    chunk_duration_sec = 240, skip_time_sec = 1, crossfade_duration_ms = 100, target_sample_rate = 44100
        'mixdown:fardrums'
    ]
}

_presets = {
    'fardrums': _preset_remix_fardrums
}

@retry(stop_max_attempt_number=3)
def do_remix_single_file(preset, input_filename, output_filename, tmp_root, niceness=0) : 
    name = osw.path_name(output_filename)
    tmpdir = osw.create_tmp_dir(tmp_root, osw.path_add_suffix(name, '_remix'))

    subpreset_names = {}
    for subpreset in preset[Tag.PresetSequence]:
        (subpreset_command, subpreset_name) = subpreset.split(':')
        subpreset_names[subpreset_command] = subpreset_name

    parser = create_argument_parser()

    separated_dir = os.path.join(tmpdir, osw.path_add_suffix(name, '_separated'))
    separate_args = parser.parse_args([
        'separate_chunked',
        '-i', input_filename,
        '-o', separated_dir,
        '-p', 'spleeter:' + subpreset_names['separate_chunked'],
        '-n', str(niceness),
        '--tmp', tmpdir,
    ])
    separated_list = separate_chunked_entrypoint(separate_args)

    mixdown_args = parser.parse_args([
        'mixdown',
        '-i', *separated_list,
        '-o', output_filename,
        '-p', 'mixdown:' + subpreset_names['mixdown'],
        '-n', str(niceness),
        '--tmp', tmpdir,
    ])
    result = mixdown_entrypoint(mixdown_args)

    osw.cleanup_tmp_files(tmpdir, separated_list)
    osw.shell_exec(f'rm -rd "{osw.shell_escape(separated_dir)}"')
    osw.shell_exec(f'rm -rd "{osw.shell_escape(tmpdir)}"')

    return result

#todo: move to sox    
def is_valid_audio_file(filepath):
    info = None
    try:
        info = sox.sox_file_info(filepath)
    except:
        pass
    return  info != None and info.samples_total > 0

def is_audio_shorter(this_audio_filename, other_audio_filename):
    tolerance_sec = 1.0

    this_info = None
    try:
        this_info = sox.sox_file_info(this_audio_filename)
    except:
        pass

    other_info = None
    try:
        other_info = sox.sox_file_info(other_audio_filename)
    except:
        pass
    
    both_valid = this_info != None and other_info != None
    this_only_invalid = this_info == None and other_info != None

    this_duration = 0.0
    other_duration = 0.0
    diff = 0.0

    if both_valid:
        this_duration = this_info.samples_total / this_info.sample_rate
        other_duration = other_info.samples_total / other_info.sample_rate
        diff = other_duration - this_duration

    shorter = this_only_invalid \
        or (both_valid and (this_duration + tolerance_sec) < other_duration)
    
    if shorter and both_valid:
        print(f'"{this_audio_filename}" is shorter ({this_duration}sec) than "{other_audio_filename}" ({other_duration}sec)\n    by {diff}sec')
    elif shorter:
        print(f'Assuming "{this_audio_filename}" is shorter than "{other_audio_filename}"')

    return shorter


def do_remix(preset, source_base_dirs, output_base_dir, output_filetype='-', jobs=1,
    overwrite_existing_shorter_audio=True, overwrite_existing_output=False, tmp_root = './tmp',
    niceness=0):

    sources_gen = osw.walk_audio_out_dir(source_base_dirs, output_base_dir)

    jobs = int(jobs)
    if jobs == 0:
        jobs = 3

    stat = {
        'files_total':0,
        'files_skipped':0,
        'files_transformed':0,
        'errors_unrecoverable':0,
        'error_file_list':[],
    }

    def remix_job(source_dir, filename, output_dir, tmp_root, niceness=0):
        stat['files_total'] += 1
        input_file = os.path.join(source_dir, filename)
        output_file = os.path.join(output_dir, filename)
        if output_filetype != '-':
            output_file = osw.path_edit(output_file, None, None, '.' + output_filetype)

        result = output_file
        file_exists = os.path.exists(output_file)
        if not file_exists \
            or (file_exists and overwrite_existing_output) \
            or (overwrite_existing_shorter_audio and is_audio_shorter(output_file, input_file)):
            
            try:
                result = do_remix_single_file(preset, input_file, output_file, tmp_root, niceness=niceness)
            except:
                stat['errors_unrecoverable'] += 1
                stat['error_file_list'].append(input_file)

            stat['files_transformed'] += 1
        else:
            Log.debug(f'Skipping "{input_file}"\n    because output exists:\n    "{output_file}"')
            stat['files_skipped'] += 1

        return result

    results = Parallel(n_jobs=jobs, prefer='threads', verbose=10)(
        delayed(remix_job)(source_dir, filename, output_dir, tmp_root, niceness=niceness)
        for _, source_dir, filename, output_dir in sources_gen)

    print(f'Remix result\n\
            files transformed: {stat["files_transformed"]}\n\
            files skipped    : {stat["files_skipped"]}\n\
            files total      : {stat["files_total"]}\n\
            \n\
            errors unrecoverable: {stat["errors_unrecoverable"]}\n\
            error file list     : {stat["error_file_list"]}\n\
            ')
    return results


def entrypoint(arguments):
    print(f'arguments = {arguments}')
    preset = _presets[arguments.preset_name]
    return do_remix(preset, arguments.input_paths, arguments.output_path, arguments.output_filetype,
        jobs=arguments.jobs_number, tmp_root=arguments.tmp_dir, niceness=arguments.niceness)
        

