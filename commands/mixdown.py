#!/usr/bin/env python
# coding: utf8

from ..utils import osw
from ..transform import sox
from enum import Enum
import os


def quess_tags_from_filenames(filenames):
    known_tags = {'_bass.':'bass', '_other.':'other', '_drums.':'drums', '_vocals.':'vocals'}
    tags = {}
    for filename in filenames:
        for pattern, known_tag in known_tags.items():
            if pattern in filename:
                if known_tag in tags:
                    raise RuntimeError(f'Both input files "{tags[known_tag]}" and "{filename}" are tagged as "{known_tag}". Only single tags are supported at the moment.')
                else:
                    tags[known_tag] = filename

    # todo: warn about untagged files
    return tags


class Tag(Enum):
    MasterOutput = 0
    PresetName = 1


_preset_fardrums = {
    Tag.PresetName: 'mixdown:fardrums',
    'drums': [
        r'gain -2 reverb -w 10 0 91 reverse reverb -w 50 0 10 reverse equalizer 63 0.048k -32 equalizer 125 0.063k +1 equalizer 250 0.125k +5 equalizer 500 0.25k +3 equalizer 1k 0.5k -3 equalizer 2k 1k -6 equalizer 4k 2k -10 equalizer 8k 4k -15 equalizer 16k 8k -15',
        r'gain -8 equalizer 125 0.063k -7 equalizer 250 0.125k -13 equalizer 500 0.25k -52 equalizer 1k 0.5k -33 equalizer 2k 1k -37 equalizer 4k 2k -40 equalizer 8k 4k -42 equalizer 16k 8k -43',
        ],
    'vocals': 'gain -12',
    'other': 'gain -12',
    'bass': 'gain -9',
    Tag.MasterOutput: r'gain -n -0.1 gain -l 3 gain -n -0.5',
}

_presets = {
    'mixdown:fardrums': _preset_fardrums
}


def do_mixdown_single(preset, source_filenames, output_filename, overwrite_existing=True, niceness=0):
    tmpdir = osw.create_tmp_dir('./tmp', osw.path_name(output_filename) + '_mixdown')

    #process each track
    transform_results = []
    for tag, transformations in preset.items():
        if isinstance(tag, str):
            if tag not in source_filenames:
                raise RuntimeError(f'Preset "{preset[Tag.PresetName]}" missing an input with tag "{tag}". Given inputs:{source_filenames}')
            else:
                if isinstance(transformations,list):
                    index = 0
                    for tr_item in transformations:
                        transformed_filename = sox.sox_transform_tmp(tmpdir,source_filenames[tag], index, tr_item, niceness=niceness)
                        transform_results.append(transformed_filename)
                        index = index + 1
                else:
                    transformed_filename = sox.sox_transform_tmp(tmpdir,source_filenames[tag], 0, transformations, niceness=niceness)
                    transform_results.append(transformed_filename)
    
    #try to create output dir if necessary
    output_dir = osw.path_root(output_filename)
    if not os.path.isdir(output_dir):
        osw.shell_exec(f'mkdir -p "{osw.shell_escape(output_dir)}"')

    #mixdown processed tracks
    sox.sox_transform(transform_results, output_filename, preset[Tag.MasterOutput], overwrite_existing=overwrite_existing, niceness=niceness)
    osw.cleanup_tmp_files(tmpdir, transform_results)
    osw.shell_exec(f'rm -d "{osw.shell_escape(tmpdir)}"')

    return output_filename
            

def entrypoint(arguments):
    print(f'arguments = {arguments}')
    preset = _presets[arguments.preset_name]
    tagged_files = quess_tags_from_filenames(arguments.input_paths)
    return do_mixdown_single(preset, tagged_files, arguments.output_filename, arguments.tmp_dir, niceness=arguments.niceness)
        

