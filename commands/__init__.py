#!/usr/bin/env python
# coding: utf8

from argparse import ArgumentParser
from tempfile import gettempdir
from os.path import exists, join

# -i opt specification.
OPT_INPUT = {
    'dest': 'input_paths',
    'nargs': '+',
    'help': 'List of input audio filenames or input directories',
    'required': True
}

# -o opt specification.
OPT_OUTPUT = {
    'dest': 'output_path',
    'default': join(gettempdir(), 'separated_audio'),
    'help': 'Path of the output directory to write audio files in'
}

# -o
OUT_OUTPUT_FILENAME = {
    'dest': 'output_filename',
    'help': 'Path to the output file to write audio',
    'required': True
}

# -j
JOBS_NUMBER = {
    'dest': 'jobs_number',
    'help': 'Jobs number for parrallel processing. Relevant if more than one input files are processed',
    'required': False,
    'default': 1
}

# -t
OPT_OUTPUT_FILETYPE = {
    'dest': 'output_filetype',
    'help': 'Extension of output files: -, flac, wav, mp3',
    'required': False,
    'default' : 'flac'
}

# --tmp
OPT_TMP_DIR = {
    'dest': 'tmp_dir',
    'help': 'Path for temporary files',
    'required': False,
    'default' : join(gettempdir(), 'audio_scripts')
}

# --verbose opt specification.
OPT_VERBOSE = {
    'action': 'store_true',
    'help': 'Shows verbose logs'
}

# -p opt specification.
OPT_TRANSFORM_PRESET = {
    'dest': 'preset_name',
    'default': 'fardrums',
    'type': str,
    'action': 'store',
    'help': 'Sound transformation preset name, only available at the moment: fardrums'
}

# -p opt specification.
OPT_PRESET_NAME = {
    'dest': 'preset_name',
    'default': 'spleeter:4stems',
    'type': str,
    'action': 'store',
    'help': 'Spleeter preset name: spleeter:4stems'
}

# -n opt specification.
OPT_NICENESS = {
    'dest': 'niceness',
    'default': 0,
    'type': int,
    'action': 'store',
    'help': 'Preferred priority of the underlying processes. From 0 (higher) to '
}


def _add_common_options(parser):
    """ Add common option to the given parser.

    :param parser: Parser to add common opt to.
    """
    parser.add_argument('--tmp', **OPT_TMP_DIR)
    parser.add_argument('--verbose', **OPT_VERBOSE)
    parser.add_argument('-n', '--niceness', **OPT_NICENESS)

    print(f'COMMON OPTIONS ADDED for {parser.prog}')


def _create_separate_chunked_parser(parser_factory):
    """ Creates an argparser for separation command

    :param parser_factory: Factory to use to create parser instance.
    :returns: Created and configured parser.
    """
    parser = parser_factory('separate_chunked', help='Separate audio files')
    _add_common_options(parser)
    parser.add_argument('-i', '--input_paths', **OPT_INPUT)
    parser.add_argument('-o', '--output_path', **OPT_OUTPUT)
    parser.add_argument('-p', '--preset', **OPT_PRESET_NAME)
    # todo: pass all unknown arguments to underlying Spleeter 'separate' command
    return parser


def _create_mixdown_parser(parser_factory):
    parser = parser_factory('mixdown', help='Mixdown tracks into single audio file')
    _add_common_options(parser)
    parser.add_argument('-i', '--input_paths', **OPT_INPUT)
    parser.add_argument('-o', '--output_filename', **OUT_OUTPUT_FILENAME)
    parser.add_argument('-p', '--preset', **OPT_TRANSFORM_PRESET)
    return parser


def _create_remix_parser(parser_factory):
    parser = parser_factory('remix', help='Separate and then mixdown audio files')
    _add_common_options(parser)
    parser.add_argument('-i', '--input_paths', **OPT_INPUT)
    parser.add_argument('-o', '--output_path', **OPT_OUTPUT)
    parser.add_argument('-t', '--output_type', **OPT_OUTPUT_FILETYPE)
    parser.add_argument('-p', '--preset', **OPT_TRANSFORM_PRESET)
    parser.add_argument('-j', '--jobs', **JOBS_NUMBER)        
    return parser    


def create_argument_parser():
    """ Creates overall command line parser for Spleeter.

    :returns: Created argument parser.
    """
    parser = ArgumentParser(prog='audio_scripts')
    subparsers = parser.add_subparsers()
    subparsers.dest = 'command'
    subparsers.required = True
    _create_separate_chunked_parser(subparsers.add_parser)
    _create_mixdown_parser(subparsers.add_parser)
    _create_remix_parser(subparsers.add_parser)
    return parser
